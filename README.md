# Traefik + SSL for Local/Production



## Easy Configuration (macOS only)

Easy configuration script available for macOS: 

1. copy .env template: `cp .env.example .env`
1. edit `.env` as necessary
1. create symbolic link for docker-compose: `ln -s docker-compose.local.yml docker-compose.yml`
1. execute `bash setup.sh`
1. follow the instruction in the setup script



## Manual configuration

### ‼️ Important note on Ubuntu 20.04 and above
- Disable and stop the systemd-resolved service:
```bash
$ sudo systemctl disable systemd-resolved.service
$ sudo systemctl stop systemd-resolved
```

- Then put the following line in the `[main]` section of your `/etc/NetworkManager/NetworkManager.conf`:
```bash
dns=default
```

- Delete the symlink `/etc/resolv.conf`
```bash
$ rm /etc/resolv.conf
```

- Restart network-manager
```bash
sudo service network-manager restart
```

#### Sources
- https://askubuntu.com/questions/907246/how-to-disable-systemd-resolved-in-ubuntu


### dnsmasq resolver

Create the resolver for the added address:

Note: /etc/resolver/**docker** filename is tld for local domain value `DOCKER_BASE_URL` set in .env 

```bash
$ sudo mkdir -v /etc/resolver && sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/docker'
```

### Configuring local CA

### mkcert Installation

follow installation specific to your OS as follows;

##### macOS

```bash
$ brew install mkcert nss
```

##### Linux

```bash
$ sudo apt install libnss3-tools mkcert
```

##### Windows (use Chocolatey)

```bash
$ choco install mkcert
```


#### Generate local CA and cert

Generate local CA store and install register that in:

```bash
$ mkcert -install
```

Generate local SSL cert:

**Note:** replace `docker` `local.docker` to match `DOCKER_BASE_URL` value set in .env

```bash
$ mkcert -key-file ./certs/key.pem -cert-file ./certs/cert.pem docker localhost '127.0.0.1' 'local.docker' '*.local.docker'
```





##### TODO:
- to create loopback alias on MacOS: `sudo ifconfig lo0 alias 127.0.0.2`

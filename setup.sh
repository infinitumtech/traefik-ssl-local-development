#!/bin/bash

# FUNCTIONS

function check_binary() {
	if type $1 &> /dev/null
	then
	    echo 1
	    return 0
	fi
	echo 0
	return 1
}

function configure_dnsmasq() {
	echo "Creating dnsmasq resolver ..."
	echo "address=/.${DOCKER_BASE_URL##*.}/127.0.0.1" >> ./dnsmasq/dnsmasq.conf
	sudo mkdir -p /etc/resolver && sudo bash -c "echo \"nameserver 127.0.0.1\" > /etc/resolver/${DOCKER_BASE_URL##*.}"
}

function configure_mkcert() {
	source .env
	echo "Creating local CA store ..."
	mkcert -install
	echo "Generating local certs ..."
	mkcert -key-file ./certs/key.pem -cert-file ./certs/cert.pem ${DOCKER_BASE_URL##*.} localhost '127.0.0.1' "${DOCKER_BASE_URL}" "*.${DOCKER_BASE_URL}"
}

function install_brew() {
	echo "brew not found installing brew ..."
	/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
	echo "brew installed! Please set brew in path as suggested above, and re-run this setup.sh script"
}

# ENTRYPOINT

# 1. check if this is macOS
clear
if [[ "$OSTYPE" != "darwin"* ]]; then
	echo "😔 Sorry! this script is currently working only on MacOS."
	echo "Please follow manual configuration section on other OS"
	exit 1
fi

# 2. check all required binaries is present, otherwise attempt to install it
BIN_MKCERT=`check_binary "mkcert"`
BIN_BREW=`check_binary "brew"`

if [[ $BIN_MKCERT = "0" ]]
then
	if [[ $BIN_BREW = "0" ]]
	then
		install_brew
		exit 1
	fi

	echo "Installing mkcert binary ..."
	brew install mkcert nss
fi

# 3. read .env values, print it, and confirm to proceed
source .env
echo "IMPORTANT! make sure your .env is correct before executing this setup script"
echo
echo "DOCKER_NAME     : '${DOCKER_NAME}'"
echo "DOCKER_BASE_URL : '${DOCKER_BASE_URL}'"
echo
echo "Are you sure to continue with this setting?"
read -p "[ press y/Y to continue, any other key to cancel ] " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
	echo
	echo "Setup cancelled!"
	exit 1
fi

# 4. create docker volumes directories
mkdir -p volumes/portainer/data &> /dev/null
mkdir -p volumes/traefik/etc/dynamic &> /dev/null
mkdir -p volumes/traefik/logs &> /dev/null

# 5. configure dnsmasq
configure_dnsmasq

# 6. configure mkcert
configure_mkcert

# 7. DONE!
echo
echo "Setup completed! 🎉"
echo
echo "Please link proper docker-compose as required:"
echo "  e.g.; For local development: ln -s docker-compose.local.yml docker-compose.yml"
echo
echo "You can execute: 'docker-compose up -d' to start this container"
echo
exit 0